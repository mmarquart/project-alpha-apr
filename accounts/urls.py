from django.urls import path
from accounts.views import user_log1n, user_logout, user_signup


urlpatterns = [
    path("login/", user_log1n, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", user_signup, name="signup"),
]
